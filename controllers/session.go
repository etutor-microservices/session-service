package controllers

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gitlab.com/etutor-microservices/session-service/database"
	"gitlab.com/etutor-microservices/session-service/models"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

var sessionCollection *mongo.Collection = database.OpenCollection(database.Client, "session")
var validate = validator.New()

func GetSessions() gin.HandlerFunc{
	return func(c *gin.Context) {
		var ctx, cancel = context.WithTimeout(context.Background(), 100*time.Second)
		result, err := sessionCollection.Find(context.TODO(), bson.M{})
		defer cancel()
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "error while fetching"})
		}
		var data []bson.M
		if err = result.All(ctx, &data); err != nil {
			log.Fatal(err)
		}
		c.JSON(http.StatusOK, data)
	}
}

func GetSessionsById() gin.HandlerFunc{
	return func(c *gin.Context) {
		var ctx, cancel = context.WithTimeout(context.Background(), 60*time.Second)
		id := c.Param("session_id")
		var data models.Session
		log.Print(id)

		err := sessionCollection.FindOne(ctx, bson.M{"_id": id}).Decode(&data)
		defer cancel()
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "error fetching data"})
			return
		}
		c.JSON(http.StatusOK, data)
	}
}

func CreateSession() gin.HandlerFunc{
	return func(c *gin.Context) {
		var data models.Session

		if err := c.BindJSON(&data); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		validationErr := validate.Struct(data)
		if validationErr != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": validationErr.Error()})
			return
		}
		var ctx, cancel = context.WithTimeout(context.Background(), 60*time.Second)

		defer cancel()
		data.SessionID = primitive.NewObjectID()
		data.From, _ = time.Parse(time.RFC3339, time.Now().Format(time.RFC3339))
		data.To, _ = time.Parse(time.RFC3339, time.Now().Format(time.RFC3339))

		result, insertErr := sessionCollection.InsertOne(ctx, data)
		if insertErr != nil {
			msg := fmt.Sprintf("Item was not created!")
			c.JSON(http.StatusInternalServerError, gin.H{"error": msg})
			return
		}
		defer cancel()
		c.JSON(http.StatusOK, result)
		defer cancel()
	}
}

func UpdateSession() gin.HandlerFunc{
	return func(c *gin.Context) {
		// var data models.Session
		var data primitive.M
		if err := c.BindJSON(&data); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		var ctx, cancel = context.WithTimeout(context.Background(), 100*time.Second)

		sessionId, _ := primitive.ObjectIDFromHex(c.Param("session_id"))
		filter := bson.M{"_id": sessionId}
		update := bson.M{"$set": data}
		result, err := sessionCollection.UpdateOne(ctx, filter, update)

		if err != nil {
			msg := "Data update failed"
			c.JSON(http.StatusInternalServerError, gin.H{"error": msg})
		}

		defer cancel()
		c.JSON(http.StatusOK, result)
	}
}

func DeleteSession() gin.HandlerFunc{
	return func(c *gin.Context) {
		var ctx, cancel = context.WithTimeout(context.Background(), 60*time.Second)

		sessionId, _ := primitive.ObjectIDFromHex(c.Param("session_id"))
		log.Printf("%v", sessionId)
		result, err := sessionCollection.DeleteOne(ctx, bson.M{"_id": sessionId})
		if err != nil {
			log.Fatal(err)
		}

		c.JSON(http.StatusOK, result.DeletedCount)
		defer cancel()
	}
}