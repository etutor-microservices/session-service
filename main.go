package main

import (
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/etutor-microservices/session-service/database"
	"gitlab.com/etutor-microservices/session-service/routes"
	"go.mongodb.org/mongo-driver/mongo"
)

var sessionCollection *mongo.Collection = database.OpenCollection(database.Client, "session")

func main () {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8888"
	}

	router := gin.New()
	router.Use(gin.Logger())
	routes.SessionRoutes(router)

	router.Run(":" + port)

}