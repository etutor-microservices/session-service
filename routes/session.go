package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/etutor-microservices/session-service/controllers"
)

func SessionRoutes(incomingRoutes *gin.Engine) {
	incomingRoutes.GET("/sessions", controllers.GetSessions())
	incomingRoutes.GET("/sessions/:session_id", controllers.GetSessionsById())
	incomingRoutes.POST("/sessions", controllers.CreateSession())
	incomingRoutes.PATCH("/sessions/:session_id", controllers.UpdateSession())
	incomingRoutes.DELETE("/sessions/:session_id", controllers.DeleteSession())
}