# eTutor Session Service

Responsible for enrollment automatic scheduling

School project that I am trying to build based on Golang Gin, RabbitMQ, gRPC.

> This project is a part of the eTutor website.

## Implementation

- Gin-Gonic .
- MongoDB.
- RabbitMQ.
- gRPC

## Preview.

We are currently developing. Stay tuned.
