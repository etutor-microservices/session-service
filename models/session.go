package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Session struct {
	SessionID			primitive.ObjectID 	`bson:"_id"`
	From 				time.Time          	`json:"from"`
	To 					time.Time          	`json:"to"`
	Description    		string             	`json:"description" validate:"max=100"`
	Attendance    		int	    	      	`json:"attendance" validate:"required"`
	Is_Finished    		bool            	`json:"is_finished" validate:"required"`
	Enrollment_ID    	string            	`json:"enrollment_id" validate:"required"`
}
